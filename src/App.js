import Cards from './components/Cards'
import base from './data.json'

function App() {
  return (
<>
<Cards base={base}></Cards>
</>
  );
}

export default App;
